# Acrylic HoloMonitor Chassis

This repository contains SolidWorks CAD files for the Mark V Holographic Video Monitor, along with an assembly of the complete system. The assembly depends on the ElectronicsSupplements and OpticsSupplements repositories within this same group, as well as two unpublished repositories: BorrowedElectronics (with downloaded CAD models for electronic components) and Cartridge (containing model files for the Modulator Cartridge, which aren't publicly released at this time).

Any questions can be directed to byuholography@gmail.com

# License

All files in this repository are Copyright 2015-2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html))

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
